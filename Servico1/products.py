import psycopg2
import ecommerce_pb2
import ecommerce_pb2_grpc
from discount import Discount

class Ecommerce(ecommerce_pb2_grpc.DiscountServicer):

    def connect_to_db(self):
        """
        Connects to the database
        :return: (psycopg2.extensions.connection)
        """
        conn = psycopg2.connect("host='postgres' dbname=teste_hash user=hash password=hash123")
        return conn

    def _get(self, cursor, table_name, id):
        """
        Get a user or a product from the database.
        :param cursor: (psycopg2.extensions.cursor) Cursor tha operates in the database.
        :param table_name: (String) Database table name
        :param id: (Int) User or product id.
        :return: (Tuple) Tuple containing the results from the query.
        """
        query = "select * from {} where id = {}".format(table_name, id)
        cursor.execute(query)
        return cursor.fetchone()

    def get_user(self, cursor, user_id):
        """
        Get a user from the database.
        :param cursor: (psycopg2.extensions.cursor) Cursor tha operates in the database.
        :param user_id: (Int) User id.
        :return: (Tuple) Tuple containg the info from a user.
        """
        return self._get(cursor, "users", user_id)

    def get_product(self, cursor, product_id):
        """
        Get a product from the database.
        :param cursor: (psycopg2.extensions.cursor) Cursor tha operates in the database.
        :param product_id: (Int) Product id.
        :return: (Tuple) Tuple containg the info from a user.
        """
        return self._get(cursor, "products", product_id)

    def ApplyDiscount(self, request, context):
        """
        Applies a discount to a product
        :param request:
        :param context:
        :return:
        """
        try:
            conn = self.connect_to_db()
            cursor = conn.cursor()

            user_id = request.user_id
            product_id = request.product_id

            user = self._get(cursor, "users", user_id)
            product = self._get(cursor, "products", product_id)

            # Caso nao receba a id do cliente ou do produto
            # Retorno uma resposta vazia.
            if not user or not product:
                return ecommerce_pb2.DiscountResponse()

            product_id = product[0]
            product_price = product[1]
            product_title = product[2]
            product_description = product[3]

            # Calculate a discount for a product.
            discount_obj = Discount()
            discount_value = discount_obj._get_discount_value(user)

            discount = None
            if discount_value:
                # Caso o produto possua desconto
                # Calculo o novo valor do produto, em centavos, e adiciono
                # na resposta a ser enviada ao cliente.
                new_price = discount_obj.calculate_new_product_price(product_price, discount_value)
                new_price = int(new_price * 100)
                pct = float(discount_value * 100)
                discount = ecommerce_pb2.DiscountValue(pct=pct,
                                                       value_in_cents=new_price)

            product_price_in_cents = product_price * 100
            product = ecommerce_pb2.Product(id=product_id,
                                            slug=product_title,
                                            description=product_description,
                                            price_in_cents=int(product_price_in_cents),
                                            discount_value=discount)

        except Exception as e:
            print(e)
            return ecommerce_pb2.DiscountResponse()

        return ecommerce_pb2.DiscountResponse(product=product)
