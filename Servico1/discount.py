from datetime import date
BLACK_FRIDAY = date(2018, 11, 25)

class Discount():

    def is_user_birthday(self, user_date_of_birth):
        """
        Check if today is the user birthday.
        :param user_date_of_birth:
        :return:(bool) True if today is the user birthday and False otherwise.
        """
        today = date.today()
        return today.month == user_date_of_birth.month and today.day == user_date_of_birth.day

    def is_black_friday(self):
        """
        Check if today is black friday.
        :return:(bool) True if today is the user birthday and False otherwise.
        """
        today = date.today()
        return today == BLACK_FRIDAY

    def _get_discount_value(self, user):
        """
        Get the discount a user can have.
        :param user: (Tuple) Tuple contaning the info from a user.
        :return: (Float) Float representing the discount given to a user.
        """
        user_date_of_birth = user[-1]

        discount = 0
        if self.is_user_birthday(user_date_of_birth):
            discount = 0.05

        if self.is_black_friday():
            discount += 0.10

        if discount > 0.10:
            discount = 0.10

        return discount

    def calculate_new_product_price(self, product_price, discount):
        """
        Calculate the new price for a product, after the discount.
        :param product_price: (Float) product price
        :param discount: (Float) Discount given to the user for a product.
        :return: (Float) The new price for a product after a discount.
        """

        new_product_price = product_price
        if discount:
            new_product_price = product_price - (product_price * discount)

        return new_product_price