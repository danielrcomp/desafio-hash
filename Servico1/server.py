import time
import grpc
import ecommerce_pb2_grpc
from concurrent import futures
from products import Ecommerce

# create a gRPC server
server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

ecommerce_pb2_grpc.add_DiscountServicer_to_server(
        Ecommerce(), server)

# listen on port 50052
print('Starting server. Listening on port 50052.')
server.add_insecure_port('[::]:50052')
server.start()

# since server.start() will not block,
# a sleep-loop is added to keep alive
try:
    while True:
        time.sleep(86400)
except KeyboardInterrupt:
    server.stop(0)