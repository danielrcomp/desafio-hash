# -*- coding: utf-8 -*-

import unittest
from freezegun import freeze_time
from datetime import date
from discount import Discount
import ecommerce_pb2

class DiscountTests(unittest.TestCase):

    def setUp(self):
        self.discount = Discount()

    def test_is_user_birthday(self):
        """
        Testing if is the user birthday.
        :return:(bool) True if is the user birthday, False otherwise
        """
        user_date_of_birth = date(1989, 12, 18)
        is_birthday = self.discount.is_user_birthday(user_date_of_birth)
        assert not is_birthday

    @freeze_time("2018-11-25")
    def test_is_black_friday(self):
        """
        Testing if today is black friday.
        This test use the freezegun lib so we can 'freeze' a specific date.
        By using this lib, all the calls to date.today() used in the code
        return the 'frozen' date.
        :return: (bool) True
        """
        assert self.discount.is_black_friday()

    def tearDown(self):
        pass

if __name__ == '__main__':
    unittest.main()
