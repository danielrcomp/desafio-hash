# -*- coding: utf-8 -*-

import unittest
from datetime import date
from freezegun import freeze_time
from products import Ecommerce
from ecommerce_pb2 import DiscountRequest
import grpc
import ecommerce_pb2_grpc

class ProductsTest(unittest.TestCase):

    def setUp(self):
        channel = grpc.insecure_channel('localhost:50052')

        self.stub = ecommerce_pb2_grpc.DiscountStub(channel)
        self.ecommerce = Ecommerce()

        self.conn = self.ecommerce.connect_to_db()
        self.cursor = self.conn.cursor()

    @freeze_time("2018-11-25")
    def test_ApplyDiscount(self):
        """
        Testing the apply discount method.
        We are 'freezing' the date, so we can test a discount
        being applied when the current date is the black friday.
        :return: (bool)
        """

        request = DiscountRequest(user_id=1, product_id=1)
        response = self.ecommerce.ApplyDiscount(request, {})
        pct = response.product.discount_value.pct

        self.assertEqual(pct, 10.0)

    def test_get_product(self):
        """
        Testing the get_product method
        :return: (bool)
        """
        product = self.ecommerce.get_product(self.cursor, 1)
        self.assertIsInstance(product, tuple)
        self.assertEqual(len(product), 4)
        self.assertEqual(product[0], 1)

    def test_get_user(self):
        """
        Testing the get_user method
        :return: (bool)
        """
        user = self.ecommerce.get_user(self.cursor, 1)
        self.assertIsInstance(user, tuple)
        self.assertEqual(len(user), 4)
        self.assertEqual(user[0], 1)

    def test_get(self):
        """
        Testing the private _get method that can fetch
        an user or a product from the database.
        :return:(bool)
        """

        product = self.ecommerce._get(self.cursor, "products", 1)
        user = self.ecommerce._get(self.cursor, "users", 1)
        self.assertIsInstance(user, tuple)
        self.assertEqual(len(user), 4)
        self.assertEqual(user[0], 1)

        self.assertIsInstance(product, tuple)
        self.assertEqual(len(product), 4)
        self.assertEqual(product[0], 1)

    def test_connect_to_db(self):
        """
        Testing the connect_to_db method.
        :return:(bool)
        """
        import psycopg2
        conn = self.ecommerce.connect_to_db()
        self.assertIsInstance(conn, psycopg2.extensions.connection)

    def tearDown(self):
        self.conn.close()

if __name__ == '__main__':
    unittest.main()
