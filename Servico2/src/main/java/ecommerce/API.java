package ecommerce;

import com.google.gson.Gson;
import com.google.gson.JsonArray;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import io.grpc.StatusRuntimeException;

import static spark.Spark.*;
import org.apache.log4j.BasicConfigurator;

public class API {

    public static void main(String[] args) {
        BasicConfigurator.configure();


        get("/products", (req, res) -> {
            Gson gson = new Gson();

            JsonArray productsListJson = new JsonArray();

            Products products = new Products();
            ArrayList<LinkedHashMap> productsList = null;
            // Se o request possui o atributo X-USER-ID
            // Então devemos calcular os descontos
            // de todos os produtos do banco de dados para
            // aquele usuário.
            if(req.headers().contains("X-USER-ID")) {
                String userId = req.headers("X-USER-ID");

                // Tento obter uma lista de produtos com
                // os descontos aplicados para o usuário.
                // Caso aconteça algum problema no
                // calculo do desconto, retorno
                // apenas a lista de produtos
                // sem desconto.
                try{
                    productsList = products.getListOfProductsWithDiscount(userId);
                }catch (StatusRuntimeException statusRuntimeException) {
                    productsList = products.getListOfProductsWithoutDiscount();
                }

            }
            // Caso o request não possua o X-USER-ID, retorno a lista de produtos
            else {
                productsList = products.getListOfProductsWithoutDiscount();
            }

            // Converto a lista de produtos para o formato JSON.
            productsListJson.add(gson.toJson(productsList));
            return productsListJson;
        });
    }
}