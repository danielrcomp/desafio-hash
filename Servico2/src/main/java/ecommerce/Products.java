package ecommerce;
import com.google.gson.Gson;
import io.grpc.StatusRuntimeException;
import io.grpc.Status;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class Products {

    private JDBC jdbc;
    private Connection connection;
    private Gson gson;
    private String user_id;

    public Products() {

        jdbc = new JDBC();
        connection = jdbc.connect();
        gson = new Gson();
    }

    /** Select all the products from the database.
     * After querying the elements, they are inserted into
     * a LinkedHashMap, so we can keep the elements in order.
     * Then they are inserted into a ArrayList.
     * @return ArrayList<LinkedHashMap>
     */
    public ArrayList getListOfProductsWithoutDiscount() {
        String sql = "SELECT * FROM products";
        ArrayList<LinkedHashMap> info_list = new ArrayList<>();

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {

                String id = rs.getString("id");
                String title = rs.getString("title");
                String desc = rs.getString("description");
                String price = rs.getString("price");
                int priceInCents = Integer.parseInt(price) * 100;

                LinkedHashMap<String, Object> product_info = new LinkedHashMap<>();
                product_info.put("id", id);
                product_info.put("title", title);
                product_info.put("description", desc);
                product_info.put("price_in_cents", priceInCents);
                info_list.add(product_info);

            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return info_list;

    }

    /** Query the database and return all of the products IDs.
     * @return ArrayList<String>.
     */
    private ArrayList getProductsIds() {
        String sql = "SELECT id FROM products";
        ArrayList<String> info_list = new ArrayList<>();

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {

                String id = rs.getString("id");
                info_list.add(id);

            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return info_list;
    }

    /** Converts a product from the server to a LinkedHashMap.
     * @param user_id User id
     * @param product_id Product id
     * @return LinkedHashMap
     */
    private LinkedHashMap getProductAsMap(int user_id, int product_id) {

        GrpcClient client = GrpcClient.getInstance();

        try {
            Ecommerce.DiscountResponse response = client.getProductResponse(user_id, product_id);

            if (!response.hasProduct()) {
                return null;
            }

            // Recuperdo o produto que o servidor
            // mandou como resposta.
            Ecommerce.Product product = response.getProduct();

            String productDescription = product.getDescription();

            String productSlug = product.getSlug();
            int priceInCents     = product.getPriceInCents();

            LinkedHashMap<String, Object> productInfo = new LinkedHashMap<>();

            productInfo.put("id", product_id);
            productInfo.put("price_in_cents", priceInCents);
            productInfo.put("title", productSlug);
            productInfo.put("description", productDescription);

            //Se o produto possui desconto
            // Também adiciono os dados do desconto
            // na LinkedHashMap.
            // Entretanto, os dados do desconto também
            // são representados como uma LinkedHashMap.
            if (product.hasDiscountValue()) {

                Ecommerce.DiscountValue discountValue = product.getDiscountValue();

                float pct = discountValue.getPct();
                int value_in_cents = discountValue.getValueInCents();

                LinkedHashMap<String, Object> discount_info = new LinkedHashMap<>();
                discount_info.put("pct", pct);
                discount_info.put("value_in_cents", value_in_cents);

                productInfo.put("discount", discount_info);

            }
            return productInfo;

        }catch (StatusRuntimeException e) {
            throw new StatusRuntimeException(Status.UNAVAILABLE);
        }
        catch (Exception e) {
            return null;
        }

    }

    /** Get all the products with or without a discount to a specific user.
     * @param userId User id
     * @return ArrayList<LinkedHashMap>.
     */
    public ArrayList getListOfProductsWithDiscount(String userId) {

        ArrayList<String> productsIds = getProductsIds();
        ArrayList<LinkedHashMap> productsList = new ArrayList();

        for(String productId  : productsIds) {

            // Como as ids são sequenciais no banco de dados
            // Devemos converte-las para Int antes de
            // calcular o desconto de produto
            // no servidor.
            int productIdAsInt = Integer.parseInt(productId);
            int userIdAsInt = Integer.parseInt(userId);

            LinkedHashMap<String, Object> productsInfo = null;
            try {
                productsInfo = getProductAsMap(userIdAsInt, productIdAsInt);

                // Se o produto apresentou algum erro
                // a ser retornado do servidor,
                // ele não é adicionado a lista
                // de produtos que será retornada.
                if (productsInfo != null) {

                    productsList.add(productsInfo);
                }

            } catch (StatusRuntimeException statusException) {
                throw new StatusRuntimeException(Status.UNAVAILABLE);
            }

        }

        return productsList;
    }

}
