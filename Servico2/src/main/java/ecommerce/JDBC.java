package ecommerce;

import com.google.gson.JsonObject;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;


public class JDBC {

    /** Connects to a database
     * @return Connection
     */
    public Connection connect() {
        Connection connection = null;

        try {
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://postgres/teste_hash", "hash",
                    "hash123");

        } catch (Exception e) {
            e.printStackTrace();
        }
    return connection;

    }


}