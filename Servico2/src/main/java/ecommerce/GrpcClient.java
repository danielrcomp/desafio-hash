package ecommerce;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

public final class GrpcClient {

    private static GrpcClient INSTANCE;

    private GrpcClient() {
    }

    /** Singleton getInstance.
     * @return
     */
    public synchronized static GrpcClient getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new GrpcClient();
        }

        return INSTANCE;
    }

    /** Get a product from the server.
     * @param userId User id
     * @param productId Product id
     * @return DiscountResponse
     */
    public Ecommerce.DiscountResponse getProductResponse(int userId, int productId) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("servico1", 50052)
                .usePlaintext()
                .build();

        DiscountGrpc.DiscountBlockingStub stub = DiscountGrpc.newBlockingStub(channel);

        Ecommerce.DiscountResponse response = stub.applyDiscount(Ecommerce.DiscountRequest
                .newBuilder()
                .setUserId(userId)
                .setProductId(productId)
                .build());
        channel.shutdown();

        return response;

    }


}