package ecommerce;

import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.4.0)",
    comments = "Source: ecommerce.proto")
public final class DiscountGrpc {

  private DiscountGrpc() {}

  public static final String SERVICE_NAME = "ecommerce.Discount";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<ecommerce.Ecommerce.DiscountRequest,
      ecommerce.Ecommerce.DiscountResponse> METHOD_APPLY_DISCOUNT =
      io.grpc.MethodDescriptor.<ecommerce.Ecommerce.DiscountRequest, ecommerce.Ecommerce.DiscountResponse>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "ecommerce.Discount", "ApplyDiscount"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ecommerce.Ecommerce.DiscountRequest.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ecommerce.Ecommerce.DiscountResponse.getDefaultInstance()))
          .build();

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static DiscountStub newStub(io.grpc.Channel channel) {
    return new DiscountStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static DiscountBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new DiscountBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static DiscountFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new DiscountFutureStub(channel);
  }

  /**
   */
  public static abstract class DiscountImplBase implements io.grpc.BindableService {

    /**
     */
    public void applyDiscount(ecommerce.Ecommerce.DiscountRequest request,
        io.grpc.stub.StreamObserver<ecommerce.Ecommerce.DiscountResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_APPLY_DISCOUNT, responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            METHOD_APPLY_DISCOUNT,
            asyncUnaryCall(
              new MethodHandlers<
                ecommerce.Ecommerce.DiscountRequest,
                ecommerce.Ecommerce.DiscountResponse>(
                  this, METHODID_APPLY_DISCOUNT)))
          .build();
    }
  }

  /**
   */
  public static final class DiscountStub extends io.grpc.stub.AbstractStub<DiscountStub> {
    private DiscountStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DiscountStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DiscountStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DiscountStub(channel, callOptions);
    }

    /**
     */
    public void applyDiscount(ecommerce.Ecommerce.DiscountRequest request,
        io.grpc.stub.StreamObserver<ecommerce.Ecommerce.DiscountResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_APPLY_DISCOUNT, getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class DiscountBlockingStub extends io.grpc.stub.AbstractStub<DiscountBlockingStub> {
    private DiscountBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DiscountBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DiscountBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DiscountBlockingStub(channel, callOptions);
    }

    /**
     */
    public ecommerce.Ecommerce.DiscountResponse applyDiscount(ecommerce.Ecommerce.DiscountRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_APPLY_DISCOUNT, getCallOptions(), request);
    }
  }

  /**
   */
  public static final class DiscountFutureStub extends io.grpc.stub.AbstractStub<DiscountFutureStub> {
    private DiscountFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DiscountFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DiscountFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DiscountFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ecommerce.Ecommerce.DiscountResponse> applyDiscount(
        ecommerce.Ecommerce.DiscountRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_APPLY_DISCOUNT, getCallOptions()), request);
    }
  }

  private static final int METHODID_APPLY_DISCOUNT = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final DiscountImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(DiscountImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_APPLY_DISCOUNT:
          serviceImpl.applyDiscount((ecommerce.Ecommerce.DiscountRequest) request,
              (io.grpc.stub.StreamObserver<ecommerce.Ecommerce.DiscountResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static final class DiscountDescriptorSupplier implements io.grpc.protobuf.ProtoFileDescriptorSupplier {
    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return ecommerce.Ecommerce.getDescriptor();
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (DiscountGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new DiscountDescriptorSupplier())
              .addMethod(METHOD_APPLY_DISCOUNT)
              .build();
        }
      }
    }
    return result;
  }
}
