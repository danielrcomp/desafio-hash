# Desafio técnico para a vaga de desenvolvedor back-end - Hash.

## Requisitos necessários:

- Docker: https://docs.docker.com/install/
- Docker Compose: https://docs.docker.com/compose/install/

## Instruções para execução:
#### Importante !!! Caso um dos comandos abaixo não seja executado, os serviços não irão funcionar!
##### Todos os comandos abaixo devem ser executados na pasta raiz do projeto.
- docker-compose build (Talvez seja necessário executar utilizando sudo: "sudo docker-compose build")
- docker-compose up (Ou docker-compose up -d para execução em background)
- docker-compose exec servico2 mvn clean package --file Servico2/pom.xml (Baixar as dependências, compilar os arquivos java e o arquivo proto)

Após rodar docker-compose up pela primeira vez, um script de para criação de tabelas e inserção de dados na base de dados será executado.
A partir da segunda execução do comando docker-compose up, o script não será executado novamente.

## Instruções para executar os serviços:
#### Importante !!! Cada um dos comandos abaixo deve ser executado em um shell diferente.
##### Todos os comandos abaixo devem ser executados na pasta raiz do projeto.
- docker-compose exec servico1 python Servico1/server.py (Executar o servidor.)
- docker-compose docker-compose exec servico2 java -jar Servico2/target/1-1.0-SNAPSHOT-jar-with-dependencies.jar

Após a execução dos dois comandos acima, uma API estará disponível.
A API possui um único endpoint para obtenção dos produtos. Existem dois modos de utilizar este endpoint:

    curl http://localhost:4567/products
    Retorna todos os produtos existentes no banco de dados.

    curl -H 'X-USER-ID: 1' http://localhost:4567/products
    X-USER-ID: É a id do user no banco de dados.
    Retorna todos produtos existentes no banco de dados com um desconto aplicado em cada produto.
    Entretanto, o desconto só é aplicado caso seja aniversário do usuário ou seja black friday.

## Testando a execução da aplicação caso o serviço 1 caia
Para este teste, basta apenas terminar a execução do servico 1, fechando o shell onde a execução dele foi iniciada ou digitando CTRL+C

## Executando testes unitários.
Para executar os testes unitários da aplicação, basta rodar o comando abaixo na pasta raiz do projeto.:
- docker-compose exec servico1 sh -c 'cd Servico1 && python -m unittest discover tests'





